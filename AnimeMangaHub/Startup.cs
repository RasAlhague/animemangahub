﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AnimeMangaHub
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

			// Home routes
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "Home1",
                    template: "AnimeMangaHub/",
					defaults: new { controller = "Home", action="Index"});
            });

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "Home2",
					template: "AnimeMangaHub/Home/",
					defaults: new { controller = "Home", action = "Index" });
			});

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "Home3",
					template: "AnimeMangaHub/home/",
					defaults: new { controller = "Home", action = "Index" });
			});

			// Anime routes
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "AnimeHome",
					template: "AnimeMangaHub/Anime/",
					defaults: new { controller = "Anime", action = "Index" });
			});

			// Manga routes
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "MangaHome",
					template: "AnimeMangaHub/Manga/",
					defaults: new { controller = "Manga", action = "Index" });
			});

			// Login routes
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "Login",
					template: "AnimeMangaHub/Login/",
					defaults: new { controller = "Login", action = "Index" });
			});
		}
    }
}
