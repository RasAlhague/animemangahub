-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 04. Feb 2019 um 19:11
-- Server-Version: 10.1.37-MariaDB
-- PHP-Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `anime_manga_hub`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_anime`
--

CREATE TABLE `tbl_anime` (
  `id_anime` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `liste_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_anime_liste`
--

CREATE TABLE `tbl_anime_liste` (
  `id_anime_liste` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ersteller_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_anwender`
--

CREATE TABLE `tbl_anwender` (
  `id_anwender` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `tbl_anwender`
--

INSERT INTO `tbl_anwender` (`id_anwender`, `username`, `password`) VALUES
(1, 'Test', 'Test');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_manga`
--

CREATE TABLE `tbl_manga` (
  `id_manga` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `liste_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_manga_liste`
--

CREATE TABLE `tbl_manga_liste` (
  `id_manga_liste` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ersteller_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_anime`
--
ALTER TABLE `tbl_anime`
  ADD PRIMARY KEY (`id_anime`);

--
-- Indizes für die Tabelle `tbl_anime_liste`
--
ALTER TABLE `tbl_anime_liste`
  ADD PRIMARY KEY (`id_anime_liste`);

--
-- Indizes für die Tabelle `tbl_anwender`
--
ALTER TABLE `tbl_anwender`
  ADD PRIMARY KEY (`id_anwender`);

--
-- Indizes für die Tabelle `tbl_manga`
--
ALTER TABLE `tbl_manga`
  ADD PRIMARY KEY (`id_manga`);

--
-- Indizes für die Tabelle `tbl_manga_liste`
--
ALTER TABLE `tbl_manga_liste`
  ADD PRIMARY KEY (`id_manga_liste`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_anime`
--
ALTER TABLE `tbl_anime`
  MODIFY `id_anime` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_anime_liste`
--
ALTER TABLE `tbl_anime_liste`
  MODIFY `id_anime_liste` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_anwender`
--
ALTER TABLE `tbl_anwender`
  MODIFY `id_anwender` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `tbl_manga`
--
ALTER TABLE `tbl_manga`
  MODIFY `id_manga` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_manga_liste`
--
ALTER TABLE `tbl_manga_liste`
  MODIFY `id_manga_liste` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
